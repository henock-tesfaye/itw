Interview project
#################

.. contents:: Table of Contents
    :local:

Main requirements
=================

Python_ 3.4+, Pipenv_ 3.5+, Django_ 2.0+

Development setup
=================

Prerequisites
-------------

You should ensure that you have a valid Python_ installation before starting working on the project.
You should also make sure that pip_ and pipenv_ are installed on your system:

On a **macOS system**, please use the following commands:

.. code-block:: shell

  $ brew install python python3
  $ easy_install pip
  $ pip install pipenv

On a **Debian-like system** (eg. Ubuntu), please use the following commands:

.. code-block:: shell

  $ sudo apt-get install python python-dev python-pip python3 python3-dev python3-pip
  $ pip install pipenv

Quickstart
----------

First initialize your local ``.env.json`` settings file. This file is used to store
environment-specific data and potential sensitive data. An example file containing development
friendly settings is provided in the repository so all you have to do to start working on the
project is a simple:

.. code-block:: shell

  $ cp .env.json.example .env.json

It should now be possible to install the project's requirements using:

.. code-block:: shell

  $ pipenv install --dev --three

Note: this project uses pipenv_ as a simple way to manage Python dependencies, developement
dependencies and virtualenvs.

If this is the first time you're working on the project, you should propably apply the Django
migrations using the ``migrate`` command and create a superuser (by default the project'll use an
SQLite database so you won't have to create a database by yourself - but feel free to update the
``.env.json`` file if you want to use another database backend):

.. code-block:: shell

  $ pipenv run python manage.py migrate
  $ pipenv run python manage.py createsuperuser

You can now run the Django development server using:

.. code-block:: shell

  $ export DJANGO_SETTINGS_MODULE=itw_project.settings.dev
  $ pipenv run python manage.py runserver

(or you can also use the ``make devserver`` shortcut)

Congrats, you're in! The development server should now be accessible at http://127.0.0.1:8000 and
the Django administration should be accessible at http://127.0.0.1:8000/admin.

Managing Python dependencies
----------------------------

Python dependencies are managed using pipenv_. This tool harnesses Pipfile (the next generation and
replacement of existing standard pip_'s ``requirements.txt`` files), pip_, and Virtualenv together
to improve workflow effeciency and best practices. It solves a common problem that can be
encountered when managing requirements using a standard ``requirements.txt`` file: maintaining both
a list of top-level dependencies a project has (often without versions specified because we want to
benefit from regular "patch" or security releases) and a complete list of all dependencies a project
has, each with exact versions specified (because we want to have **reproducible** environments).

Pipenv_ automatically adds / removes packages from the ``Pipfile`` file as we install / uninstall
packages. The ``lock`` command generates a lockfile (``Pipfile.lock``) which contains all the
dependencies of the project (the top-level dependencies **and** the underlying dependencies) with
exact versions.

Packages installation can be done using the ``pipenv install`` command, as follows. As noted
previously, it is also necessary to "lock" the requirements using the ``lock`` command:

.. code-block:: shell

  $ pipenv install mypackage
  # Locks the requirements...
  $ pipenv lock -r > requirements.freeze

Running the test suite
======================

The test suite can be run using the following commands:

.. code-block:: shell

  $ make tests  # Runs all the available tests (Python, JS, ...)
  $ make tests_python  # Runs the Python test suite
  $ make spec_python  # Runs the Python test suite in "spec" mode

For code coverage, the following command can be used:

.. code-block:: shell

  $ make coverage  # Outputs code coverage report for the Python and JS codebase
  $ make coverage_python  # Outputs code coverage report for the Python codebase

Finally, quality assurance checks can be executed using:

.. code-block:: shell

  $ make qa  # Runs QA checks for the Python and JS codebase
  $ make isort  # Runs import sort checks
  $ make lint  # Runs lint checks (Python and JS)

Other actions are available in the ``Makefile``. Please refer to this file file for a full list of
the available actions.

.. _Django: https://www.djangoproject.com/
.. _pip: https://pip.pypa.io/en/stable/
.. _Pipenv: https://github.com/kennethreitz/pipenv
.. _pipenv: https://github.com/kennethreitz/pipenv
.. _Python: https://www.python.org
