from __future__ import unicode_literals

import pytest
from bs4 import BeautifulSoup
from django.test import Client
from django.urls import reverse

from itw.apps.thing import models


def things_list_test_maker(url, manager='objects'):

    @pytest.mark.django_db
    def inner():
        client = Client()
        response = client.get(url)
        soup = BeautifulSoup(response.content, features="html.parser")

        assert soup.find('a', {
            'href': reverse('thing:thing_create'),
        }), "Create button missing"
        assert soup.find('a', {
            'href': reverse('thing:thing_list_cool'),
        }), "Cool things button missing"
        assert soup.find('a', {
            'href': reverse('thing:thing_list_bad'),
        }), "Bad things button missing"

        columns = soup.select('table.table > thead th')
        column_names = [column.text for column in columns]
        expected_names = ['#', 'Name', 'Coolness', 'Description', 'Actions']
        assert column_names == expected_names, "Column name has not matched"

        content_rows = soup.select('table.table > tbody tr')
        assert len(content_rows) == 1
        assert content_rows[0].text.strip() == 'There are no things!'

        for i in range(-3, 3):
            models.Thing.objects.create(name='TEST %d' % i,
                                        coolness=i,
                                        description='TEST %d' % i)

        response = client.get(url)
        soup = BeautifulSoup(response.content, features="html.parser")
        content_body = soup.select_one('table.table > tbody')
        things = getattr(models.Thing, manager).order_by('-coolness')
        content_rows = content_body.select('tr')
        assert len(content_rows) == things.count(
        ), "Number of rows doesn't much the number of things"

        for thing, row in zip(things, content_rows):
            columns = row.select('td')
            assert thing.name in columns[0].text
            assert str(thing.coolness) in columns[1].text
            assert thing.description in columns[2].text
            detail, update, delete = columns[3].find_all('a')

            assert detail['href'] == thing.get_absolute_url()
            assert update['href'] == reverse(
                'thing:thing_update', kwargs={'pk': thing.pk})
            assert delete['href'] == reverse(
                'thing:thing_delete', kwargs={'pk': thing.pk})

    return inner


test_things_list = things_list_test_maker(reverse('thing:thing_list'))
test_cool_things_list = things_list_test_maker(
    reverse('thing:thing_list_cool'), 'cool_objects')
test_bad_things_list = things_list_test_maker(
    reverse('thing:thing_list_bad'), 'bad_objects')


@pytest.mark.django_db
def test_thing_detail():
    thing = models.Thing.objects.create(name='TEST', coolness=0)
    client = Client()
    response = client.get(thing.get_absolute_url())
    assert 'Name: TEST' in str(response.content)
    assert 'Coolness: 0' in str(response.content)
    assert 'Description:' in str(response.content)


@pytest.mark.django_db
def test_thing_create():
    client = Client()
    url = reverse('thing:thing_create')
    response = client.get(url)
    soup = BeautifulSoup(response.content, features="html.parser")
    assert soup.find('input', attrs={'name': 'name', 'type': 'text'})
    assert soup.find('input', attrs={'name': 'coolness', 'type': 'number'})
    assert soup.find('textarea', attrs={'name': 'description'})

    assert models.Thing.objects.count() == 0
    response = client.post(url, {'name': 'TEST',
                                 'coolness': 1,
                                 'description': 'TEST'})
    assert models.Thing.objects.count() == 1, "Thing creation has failed"

    success_message = "%(name)s was created successfully" % {
        'name': models.Thing.objects.last().name,
    }
    assert success_message in str(client.get(
        response['location']).content), "Message was not shown."


@pytest.mark.django_db
def test_thing_update():
    thing = models.Thing.objects.create(name='TEST',
                                        coolness=0,
                                        description='TEST')
    client = Client()
    url = reverse('thing:thing_update', kwargs={'pk': thing.pk})
    response = client.get(url)
    soup = BeautifulSoup(response.content, features="html.parser")
    assert soup.find('input', attrs={'name': 'name',
                                     'type': 'text',
                                     'value': 'TEST'})
    assert soup.find('input', attrs={'name': 'coolness',
                                     'type': 'number',
                                     'value': 0})
    assert soup.find('textarea', attrs={
        'name': 'description'
    }).text.strip() == 'TEST'

    response = client.post(url, {'name': 'TEST2',
                                 'coolness': 2,
                                 'description': 'TEST2'})
    updated_thing = models.Thing.objects.get(pk=thing.pk)
    assert updated_thing.name == 'TEST2', "Name update has failed"
    assert updated_thing.coolness == 2, "Coolness update has failed"
    assert updated_thing.description == 'TEST2', "Description update has failed"

    success_message = "%(name)s was updated successfully" % {
        'name': updated_thing.name,
    }
    assert success_message in str(client.get(
        response['location']).content), "Message was not shown."


@pytest.mark.django_db
def test_thing_delete():
    thing = models.Thing.objects.create(name='TEST', coolness=0)
    client = Client()
    url = reverse('thing:thing_delete', kwargs={'pk': thing.pk})
    response = client.get(url)
    assert 'Are you sure you want to delete "TEST"?' in str(response.content)

    response = client.post(url, {})
    assert models.Thing.objects.count() == 0
    success_message = "%(name)s was deleted successfully" % {
        'name': thing.name,
    }
    assert success_message in str(client.get(
        response['location']).content), "Message was not shown."
