"""
    Common abstract models
    ======================

    This module defines common abstract models that can be used (or combined) when defining new
    models.

"""

from django.db import models
from django.utils.translation import ugettext_lazy as _


class DatedModel(models.Model):
    """ Represents a model associated with created/updated datetime fields.

    An abstract base class model that provides a created and a updated fields to store creation date
    and last updated date.

    """

    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Creation date'))
    updated = models.DateTimeField(auto_now=True, verbose_name=_('Update date'))

    class Meta:
        abstract = True
