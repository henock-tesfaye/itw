"""
    Thing models
    ============

    This module defines dummy models used for tests only!

"""

from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from itw.common.abstract_models import DatedModel


class FilterManager(models.Manager):
    """ An Manager that returns a filtered queryset """

    def __init__(self, **filters):
        """
        :filters: keyword arguments that would later be passed to .filter
        """
        self.filters = filters
        super(FilterManager, self).__init__()

    def get_queryset(self):
        """
        :returns: filtered queryset based on filters passed to the manager
        """
        return super(FilterManager, self).get_queryset().filter(**self.filters)


class Thing(DatedModel):
    """ Represents a dummy thing! """

    name = models.CharField(
        max_length=128, verbose_name=_('Name'), unique=True)
    coolness = models.SmallIntegerField(verbose_name=_('Coolness'))
    description = models.TextField(
        blank=True,
        null=True,
        verbose_name=_('Description')
    )
    objects = models.Manager()
    cool_objects = FilterManager(coolness__gte=0)
    bad_objects = FilterManager(coolness__lt=0)

    class Meta:
        ordering = ('coolness', )
        verbose_name = _('Thing')
        verbose_name_plural = _('Things')

    def get_absolute_url(self):
        return reverse('thing:thing_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name
