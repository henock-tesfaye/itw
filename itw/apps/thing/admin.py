"""
    Thing model admin definitions
    =============================

    This module defines admin classes used to populate the Django administration dashboard.

"""

from django.contrib import admin

from .models import Thing


@admin.register(Thing)
class BuyerAdmin(admin.ModelAdmin):
    """ The thing model admin. """

    list_display = ('name', 'coolness', 'description', )
