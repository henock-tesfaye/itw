from django.conf.urls import include, url
from django.views.generic import RedirectView

from . import views


urlpatterns = [
    url(r'^$', RedirectView.as_view(pattern_name='thing:thing_list'), name='home'),
    url(r'^thing/', include('itw.public.thing.urls', namespace='thing')),
    url(r'^bad/$', views.bad),
]
