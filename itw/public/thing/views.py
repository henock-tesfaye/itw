from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, DetailView, ListView, UpdateView

from itw.apps.thing.models import Thing


class ThingListView(ListView):
    """ Allows to browse a list of dummy things! """

    context_object_name = 'things'
    model = Thing
    ordering = ('-coolness', )
    template_name = 'thing/thing_list.html'

    def get_context_data(self, *args, **kwargs):
        context_data = super(ThingListView, self).get_context_data(*args,
                                                                   **kwargs)
        context_data['view_name'] = self.request.resolver_match.view_name
        return context_data


class ThingCreateView(SuccessMessageMixin, CreateView):
    """ Allows the creation of a dummy thing! """

    model = Thing
    template_name = 'thing/thing_create.html'
    fields = ('name', 'coolness', 'description')
    success_url = reverse_lazy('thing:thing_list')
    success_message = "%(name)s was created successfully"


class ThingDetailView(DetailView):
    """ Allows the display of detail of a dummy thing! """

    model = Thing
    context_object_name = 'thing'
    template_name = 'thing/thing_detail.html'


class ThingUpdateView(SuccessMessageMixin, UpdateView):
    """ Allows the updating of a dummy thing! """

    model = Thing
    context_object_name = 'thing'
    template_name = 'thing/thing_update.html'
    fields = ('name', 'coolness', 'description')
    success_url = reverse_lazy('thing:thing_list')
    success_message = "%(name)s was updated successfully"


class ThingDeleteView(SuccessMessageMixin, DeleteView):
    """ Allows the deletion of a dummy thing! """

    model = Thing
    context_object_name = 'thing'
    template_name = 'thing/thing_delete.html'
    success_url = reverse_lazy('thing:thing_list')
    success_message = "%(name)s was deleted successfully"

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()
        messages.success(self.request, self.success_message % obj.__dict__)
        return super(ThingDeleteView, self).delete(request, *args, **kwargs)


class CoolThingListView(ThingListView):
    """ Allows to browse a list of cool dummy things! """

    queryset = Thing.cool_objects.all()


class BadThingListView(ThingListView):
    """ Allows to browse a list of bad dummy things! """

    queryset = Thing.bad_objects.all()
