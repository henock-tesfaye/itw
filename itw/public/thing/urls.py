from django.conf.urls import url

from . import views


app_name = 'thing'

urlpatterns = [
    url(r'^$', views.ThingListView.as_view(), name='thing_list'),
    url(r'^cool/$', views.CoolThingListView.as_view(), name='thing_list_cool'),
    url(r'^bad/$', views.BadThingListView.as_view(), name='thing_list_bad'),
    url(r'^create/$', views.ThingCreateView.as_view(), name='thing_create'),
    url(r'^(?P<pk>\d+)/$', views.ThingDetailView.as_view(),
        name='thing_detail'),
    url(r'^(?P<pk>\d+)/update/$', views.ThingUpdateView.as_view(),
        name='thing_update'),
    url(r'^(?P<pk>\d+)/delete/$', views.ThingDeleteView.as_view(),
        name='thing_delete'),
]
