from django.contrib.admin.views.decorators import staff_member_required


@staff_member_required
def bad(request):  # pragma: no cover
    """ Simulates a server error. Useful to test error logging and propagation. """
    1 / 0
