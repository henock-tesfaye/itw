Questions:
1. Install the project on your laptop and launch the server (nb: the code source of the project will be made available to you when you start the test). From there you should be able to access the Django administration interface and start creating some Thing instances

2. As you can experience, it is possible to create multiple instances of Thing with the same name. Update the Thing model so that it is not possible to do so anymore

3. In order to describe more precisely each thing, add a “description” field to the Thing model. This field should not be mandatory. The description should also be made visible on the table that is displayed on the home page of the application

4. We don’t want to use the Django administration interface to manipulate our Thing instances. Add a view allowing to create new Thing instances. A link allowing to access the related form should be made available on the home page of the application

5. Add a column “Actions” on the table of Thing that is present on the home page of the application

6. Create a view allowing to update an instance of Thing. We want to be able to edit the name, description and coolness of a given Thing instance. The related form should be made available through a link present in the “Actions” column for each thing displayed on the table

7. Create a view allowing to delete an instance of Thing. The related confirmation form should be made available through a link present in the “Actions” column for each thing displayed on the table

8. For each view / form created previously we want to be able to display a notification message to the user when the related action (creation, update or deletion) is performed. Add this behaviour to the application

9. Create two managers: one for retrieving cool things (Thing instances with a coolness value greater than or equal to 0) and one for retrieving bad things (Thing instances with a coolness value less than 0). These two managers should be associated with the Thing model

10. Create two new views: one for listing cool things and one another for listing bad things. You should make use of the managers created in the previous question here. These two new lists should be displayed as tables and should be made accessible in the global navigation bar.
