"""
    Development Django settings
    ===========================

    This file imports the ``base`` settings and can add or modify previously defined settings to
    alter the configuration of the application for development purposes.

    For more information on this file, see https://docs.djangoproject.com/en/dev/topics/settings/
    For the full list of settings and their values, see
    https://docs.djangoproject.com/en/dev/ref/settings/

"""

from .dev import *  # noqa: F403

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------

# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:'
    }
}

# ENV-SPECIFIC CONFIGURATION
# ------------------------------------------------------------------------------

try:
    # Allow the use of a settings module named "settings_env" that is not
    # contributed to the repository (only when dev settings are in use!).
    from .settings_env import *  # noqa
except ImportError:
    pass
