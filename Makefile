PROJECT_PACKAGE := itw
PROJECT_CONFIGURATION_PACKAGE := $(PROJECT_PACKAGE)_project
DJANGO_SETTINGS_MODULE := $(PROJECT_CONFIGURATION_PACKAGE).settings.dev

.PHONY: devserver qa lint lint_python isort isort_python tests tests_python spec spec_python


init:
	pipenv install --dev --three


# DEVELOPMENT
# ~~~~~~~~~~~
# The following rules can be used during development in order to launch development server, generate
# locales, etc.
# --------------------------------------------------------------------------------------------------

devserver:
	pipenv run python manage.py runserver 0.0.0.0:8000 --settings=$(DJANGO_SETTINGS_MODULE)

shell:
	pipenv run python manage.py shell --settings=$(DJANGO_SETTINGS_MODULE)

migrations:
	pipenv run python manage.py makemigrations --settings=$(DJANGO_SETTINGS_MODULE) ${ARG}

migrate:
	pipenv run python manage.py migrate --settings=$(DJANGO_SETTINGS_MODULE)

superuser:
	pipenv run python manage.py createsuperuser --settings=$(DJANGO_SETTINGS_MODULE)


# QUALITY ASSURANCE
# ~~~~~~~~~~~~~~~~~
# The following rules can be used to check code quality, import sorting, etc.
# --------------------------------------------------------------------------------------------------

qa: lint isort

# Code quality checks (eg. flake8, etc).
lint: lint_python
lint_python:
	pipenv run flake8

# Import sort checks.
isort: isort_python
isort_python:
	pipenv run isort --check-only --recursive --diff $(PROJECT_PACKAGE) $(PROJECT_CONFIGURATION_PACKAGE)


# TESTING
# ~~~~~~~
# The following rules can be used to trigger tests execution and produce coverage reports.
# --------------------------------------------------------------------------------------------------

# Just runs all the tests!
tests: tests_python
tests_python:
	pipenv run py.test

# Collects code coverage data.
coverage: coverage_python
coverage_python:
	pipenv run py.test --cov-report term-missing --cov $(PROJECT_PACKAGE)

# Run the tests in "spec" mode.
spec: spec_python
spec_python:
	pipenv run py.test --spec -p no:sugar
